package my.personal.adapterapplication;

import java.io.Serializable;

public class Product implements Serializable {
    private String name;
    private String description;

    public Product() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
