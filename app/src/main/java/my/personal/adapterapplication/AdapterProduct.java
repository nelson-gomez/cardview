package my.personal.adapterapplication;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AdapterProduct extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Product> productList;

    private class CustomViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDetail;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.textViewTitle);
            textViewDetail = itemView.findViewById(R.id.textViewDetail);
        }
    }

    public AdapterProduct(List<Product> productList) {
        super();
        this.productList = productList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_product, viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        Product product = productList.get(position);
        customViewHolder.textViewTitle.setText(product.getName());
        customViewHolder.textViewDetail.setText(product.getDescription());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
